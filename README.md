# fRSys

## Overview

fRSys provides Fortran users with a way to use the RSys C library.
While most of the types and functions in RSys are just made accessible through
Fortran's ISO_C_BINDING intrinsic module, part of the RSys features are provided
as C-preprocessor macros, others are implemented as Fortran procedures, and a 
few functionalities are part of the frsys companion C-library.

## Install

### Pre-requisites

fRSys is compatible GNU/Linux as well as Microsoft Windows 7 and later, both
in 64-bits. 
It was successfully built with the [GNU Compiler Collection](https://gcc.gnu.org) 
(versions 4.7 and later), with the flags:
[-std=f2008ts](https://gcc.gnu.org/onlinedocs/gfortran/Fortran-Dialect-Options.html)
(Fortran 2008 standard including the additions of TS 29113 and TS 18508), 
[-ffree-form](https://gcc.gnu.org/onlinedocs/gfortran/Fortran-Dialect-Options.html)
(free form layout),
[-ffree-line-length-none](https://gcc.gnu.org/onlinedocs/gfortran/Fortran-Dialect-Options.html)
(no limit on source file's line length), and
[-cpp](https://gcc.gnu.org/onlinedocs/gfortran/Preprocessing-Options.html)
(C-preprocessing enabled on source files).
No compiler was tested on Windows; it is assumed that Intel Fortran could be 
successfully used.
It relies on [CMake](http://www.cmake.org) and the 
[RCMake](https://gitlab.com/vaplv/rcmake/) package to build. 
It also depends on the [RSys](https://gitlab.com/vaplv/rsys/) library.

### How to build

First ensure that CMake and a C and a Fortran compiler with Fortran 2008 + 
TS 29113 support (i.e. gfortran 4.7 or later) are installed on your system. 
Then install the [RCMake](https://gitlab.com/vaplv/rcmake.git) package as 
well as the [RSys](https://gitlab.com/vaplv/rsys.git) library.
Finally, generate the project from the `cmake/CMakeLists.txt` file by 
appending to the `CMAKE_PREFIX_PATH` variable the `<RCMAKE_DIR>/lib/cmake` 
and `<RSYS_DIR>` directories, where `<RCMAKE_DIR>` and `<RSYS_DIR>` are the
install directories of the RCMake package and the RSys library, respectively.
The resulting project can be edited, built, tested and installed as any CMake 
project (Refer to the [CMake documentation](https://cmake.org/documentation) 
for further informations on CMake).

Example on a GNU/Linux system:

    ~ $ git clone https://gitlab.com/meso-star/frsys.git
    ~ $ mkdir frsys/build && cd frsys/build
    ~/frsys/build $ cmake -G "Unix Makefiles" \
    > -DCMAKE_PREFIX_PATH="<RCMAKE_DIR>/lib/cmake;<RSYS_DIR>" \
    > -DCMAKE_INSTALL_PREFIX=<FRSYS_INSTALL_DIR> \
    > ../cmake
    ~/frsys/build $ make && make test
    ~/frsys/build $ make install

with `<FRSYS_INSTALL_DIR>` the directory in which fRSys is going to be
installed.

### How to use

Using fRSys requires a fortran compiler supporting the Fortran 2008 standard
and TS 29113 addition, free form layout, unlimited line length for source 
code, and C-preprocessing of Fortran files.

When using gfortran, the adequate options are -std=f2008ts -ffree-form 
-ffree-line-length-none -cpp.

Provided include files (*.inc) must be included using #include, as the Fortran 
INCLUDE statement doesn't allow C-like preprocessor directives in included 
files.

The frsys companion library should be added at the link stage.

For an example of use, please look at the provided test programs.

## License

fRSys is Copyright (C) |Meso|Star> 2015-2016 (<contact@meso-star.com>). It
is a free software released under the [OSI](http://opensource.org)-approved
CeCILL license. You are welcome to redistribute it under certain conditions;
refer to the COPYING files for details.
