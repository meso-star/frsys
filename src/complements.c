/* Copyright (C) |Meso|Star> 2015-2016 (contact@meso-star.com)
 *
 * This software is a collection of files whose purpose is to give access
 * to the RSys library from Fortran programs
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */

#include "complements.h"
#include <rsys/float3.h>

_Bool is_aligned(void *addr, size_t algnt) {
  return IS_ALIGNED(addr, algnt);
}

size_t align_size(size_t size, size_t algnt) {
  return ALIGN_SIZE(size, algnt);
}

const size_t SIZEOF_C_CHAR__ = sizeof(char);
const size_t SIZEOF_C_INT8_T__ = sizeof(int8_t);
const size_t SIZEOF_C_INT16_T__ = sizeof(int16_t);
const size_t SIZEOF_C_INT32_T__ = sizeof(int32_t);
const size_t SIZEOF_C_INT64_T__ = sizeof(int64_t);
const size_t SIZEOF_C_FLOAT__ = sizeof(float);
const size_t SIZEOF_C_DOUBLE__ = sizeof(double);
const size_t SIZEOF_C_LONG_DOUBLE__ = sizeof(long double);
const size_t SIZEOF_C_FLOAT_COMPLEX__ = sizeof(float _Complex);
const size_t SIZEOF_C_DOUBLE_COMPLEX__ = sizeof(double _Complex);
const size_t SIZEOF_C_LONG_DOUBLE_COMPLEX__ = sizeof(long double _Complex);
const size_t SIZEOF_C_BOOL__ = sizeof(_Bool);

const size_t ALIGNOF_C_CHAR__ = ALIGNOF(char);
const size_t ALIGNOF_C_INT8_T__ = ALIGNOF(int8_t);
const size_t ALIGNOF_C_INT16_T__ = ALIGNOF(int16_t);
const size_t ALIGNOF_C_INT32_T__ = ALIGNOF(int32_t);
const size_t ALIGNOF_C_INT64_T__ = ALIGNOF(int64_t);
const size_t ALIGNOF_C_FLOAT__ = ALIGNOF(float);
const size_t ALIGNOF_C_DOUBLE__ = ALIGNOF(double);
const size_t ALIGNOF_C_LONG_DOUBLE__ = ALIGNOF(long double);
const size_t ALIGNOF_C_FLOAT_COMPLEX__ = ALIGNOF(float _Complex);
const size_t ALIGNOF_C_DOUBLE_COMPLEX__ = ALIGNOF(double _Complex);
const size_t ALIGNOF_C_LONG_DOUBLE_COMPLEX__ = ALIGNOF(long double _Complex);
const size_t ALIGNOF_C_BOOL__ = ALIGNOF(_Bool);

_Bool mem_area_overlap(void *ptrA, size_t sizeA, void *ptrB, size_t sizeB) {
  return MEM_AREA_OVERLAP(ptrA, sizeA, ptrB, sizeB);
}

void F3_CROSS(float dst[3], const float a[3], const float b[3]) {
  (void)f3_cross(dst, a, b);
}

float F3_NORMALIZE(float dst[3], const float src[3]) {
  return f3_normalize(dst, src);
}

_Bool F3_EQ_EPS(const float* a, const float* b, const float eps) {
  return f3_eq_eps(a, b, eps);
}
