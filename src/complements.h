/* Copyright (C) |Meso|Star> 2015-2016 (contact@meso-star.com)
 *
 * This software is a collection of files whose purpose is to give access
 * to the RSys library from Fortran programs
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */

#ifndef RSYS_COMPLEMENTS_H
#define RSYS_COMPLEMENTS_H

#include <rsys/rsys.h>

/* Library symbol management */
#if defined(FRSYS_SHARED_BUILD) /* Build shared library */
  #define FRSYS_API extern EXPORT_SYM
#elif defined(FRSYS_STATIC) /* Use/build static library */
  #define FRSYS_API extern LOCAL_SYM
#else /* Use shared library */
  #define FRSYS_API extern IMPORT_SYM
#endif

BEGIN_DECLS

/*******************************************************************************
 * sizeof replacement
 ******************************************************************************/
FRSYS_API const size_t SIZEOF_C_CHAR__;
FRSYS_API const size_t SIZEOF_C_INT8_T__;
FRSYS_API const size_t SIZEOF_C_INT16_T__;
FRSYS_API const size_t SIZEOF_C_INT32_T__;
FRSYS_API const size_t SIZEOF_C_INT64_T__;
FRSYS_API const size_t SIZEOF_C_FLOAT__;
FRSYS_API const size_t SIZEOF_C_DOUBLE__;
FRSYS_API const size_t SIZEOF_C_LONG_DOUBLE__;
FRSYS_API const size_t SIZEOF_C_FLOAT_COMPLEX__;
FRSYS_API const size_t SIZEOF_C_DOUBLE_COMPLEX__;
FRSYS_API const size_t SIZEOF_C_LONG_DOUBLE_COMPLEX__;
FRSYS_API const size_t SIZEOF_C_BOOL__;

/*******************************************************************************
 * Data alignment macros replacement
 ******************************************************************************/

FRSYS_API _Bool is_aligned(void *addr, size_t algnt);

FRSYS_API size_t align_size(size_t size, size_t algnt);

FRSYS_API const size_t ALIGNOF_C_CHAR__;
FRSYS_API const size_t ALIGNOF_C_INT8_T__;
FRSYS_API const size_t ALIGNOF_C_INT16_T__;
FRSYS_API const size_t ALIGNOF_C_INT32_T__;
FRSYS_API const size_t ALIGNOF_C_INT64_T__;
FRSYS_API const size_t ALIGNOF_C_FLOAT__;
FRSYS_API const size_t ALIGNOF_C_DOUBLE__;
FRSYS_API const size_t ALIGNOF_C_LONG_DOUBLE__;
FRSYS_API const size_t ALIGNOF_C_FLOAT_COMPLEX__;
FRSYS_API const size_t ALIGNOF_C_DOUBLE_COMPLEX__;
FRSYS_API const size_t ALIGNOF_C_LONG_DOUBLE_COMPLEX__;
FRSYS_API const size_t ALIGNOF_C_BOOL__;

/*******************************************************************************
 * Miscellaneous
 ******************************************************************************/
FRSYS_API _Bool mem_area_overlap(void *ptrA, size_t sizeA, void *ptrB, size_t sizeB);

/*******************************************************************************
 * float3
 ******************************************************************************/
FRSYS_API void F3_CROSS(float dst[3], const float a[3], const float b[3]);

FRSYS_API float F3_NORMALIZE(float dst[3], const float src[3]);

FRSYS_API _Bool F3_EQ_EPS(const float* a, const float* b, const float eps);

END_DECLS

#endif

