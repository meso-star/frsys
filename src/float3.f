! Copyright (C) |Meso|Star> 2015 (contact@meso-star.com)
!
! This software is a collection of files whose purpose is to give access
! to the RSys library from Fortran programs
!
! This software is governed by the CeCILL license under French law and
! abiding by the rules of distribution of free software. You can use,
! modify and/or redistribute the software under the terms of the CeCILL
! license as circulated by CEA, CNRS and INRIA at the following URL
! "http://www.cecill.info".
!
! As a counterpart to the access to the source code and rights to copy,
! modify and redistribute granted by the license, users are provided only
! with a limited warranty and the software's author, the holder of the
! economic rights, and the successive licensors have only limited
! liability.
!
! In this respect, the user's attention is drawn to the risks associated
! with loading, using, modifying and/or developing or reproducing the
! software by the user in light of its specific status of free software,
! that may mean that it is complicated to manipulate, and that also
! therefore means that it is reserved for developers and experienced
! professionals having in-depth computer knowledge. Users are therefore
! encouraged to load and test the software's suitability as regards their
! requirements in conditions enabling the security of their systems and/or
! data to be ensured and, more generally, to use and operate it in the
! same conditions as regards security.
!
! The fact that you are presently reading this means that you have had
! knowledge of the CeCILL license and that you accept its terms.

MODULE RSYS_FLOAT3_BINDING

INTERFACE

! normalize a C float[3]: dst = src / |src|, return = |src|
! if src==0, dst is left unchanged
FUNCTION F3_NORMALIZE(dst, src) BIND(C, NAME='F3_NORMALIZE')
  USE ISO_C_BINDING, ONLY: C_FLOAT
  IMPLICIT NONE
  REAL(C_FLOAT), INTENT(OUT) :: dst(3)
  REAL(C_FLOAT), INTENT(IN) :: src(3)
  REAL(C_FLOAT) :: F3_NORMALIZE
END FUNCTION F3_NORMALIZE

! cross product between 2 C float[3]: dst = a*b
SUBROUTINE F3_CROSS(dst, a, b) BIND(C, NAME='F3_CROSS')
  USE ISO_C_BINDING, ONLY: C_FLOAT
  IMPLICIT NONE
  REAL(C_FLOAT), INTENT(OUT) :: dst(3)
  REAL(C_FLOAT), INTENT(IN) :: a(3)
  REAL(C_FLOAT), INTENT(IN) :: b(3)
END SUBROUTINE F3_CROSS

! check if 2 C float[3] are componentwise closer than eps
FUNCTION F3_EQ_EPS(a, b, eps) BIND(C, NAME='F3_EQ_EPS')
  USE ISO_C_BINDING, ONLY: C_BOOL, C_FLOAT
  IMPLICIT NONE
  REAL(C_FLOAT), INTENT(IN) :: a(3)
  REAL(C_FLOAT), INTENT(IN) :: b(3)
  REAL(C_FLOAT), INTENT(IN), VALUE :: eps
  LOGICAL(C_BOOL) :: F3_EQ_EPS
END FUNCTION F3_EQ_EPS

END INTERFACE

END MODULE RSYS_FLOAT3_BINDING
