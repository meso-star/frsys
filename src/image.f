! Copyright (C) |Meso|Star> 2015 (contact@meso-star.com)
!
! This software is a collection of files whose purpose is to give access
! to the RSys library from Fortran programs
!
! This software is governed by the CeCILL license under French law and
! abiding by the rules of distribution of free software. You can use,
! modify and/or redistribute the software under the terms of the CeCILL
! license as circulated by CEA, CNRS and INRIA at the following URL
! "http://www.cecill.info".
!
! As a counterpart to the access to the source code and rights to copy,
! modify and redistribute granted by the license, users are provided only
! with a limited warranty and the software's author, the holder of the
! economic rights, and the successive licensors have only limited
! liability.
!
! In this respect, the user's attention is drawn to the risks associated
! with loading, using, modifying and/or developing or reproducing the
! software by the user in light of its specific status of free software,
! that may mean that it is complicated to manipulate, and that also
! therefore means that it is reserved for developers and experienced
! professionals having in-depth computer knowledge. Users are therefore
! encouraged to load and test the software's suitability as regards their
! requirements in conditions enabling the security of their systems and/or
! data to be ensured and, more generally, to use and operate it in the
! same conditions as regards security.
!
! The fact that you are presently reading this means that you have had
! knowledge of the CeCILL license and that you accept its terms.

MODULE RSYS_IMAGE_BINDING

INTERFACE

! write a buffer of pixels into a ppm file
! width and height are the dimensions of the picture
! returns a code from res_T enum (see rsys.inc)
FUNCTION image_ppm_write(path, width, height, bytes_per_pixel, buffer) BIND(C, NAME='image_ppm_write')
  USE ISO_C_BINDING, ONLY: C_CHAR, C_INT8_T, C_INT, C_PTR
  IMPLICIT NONE
  CHARACTER(C_CHAR), INTENT(IN) :: path(*)
  INTEGER(C_INT), INTENT(IN), VALUE :: width
  INTEGER(C_INT), INTENT(IN), VALUE :: height
  INTEGER(C_INT), INTENT(IN), VALUE :: bytes_per_pixel
  INTEGER(C_INT8_T), INTENT(IN) :: buffer(*)
  INTEGER(C_INT) :: image_ppm_write
END FUNCTION image_ppm_write

! same as above, but taking a C FILE* as argument
! where the previous takes a file name
FUNCTION image_ppm_write_stream(stream, width, height, bytes_per_pixel, buffer) BIND(C, NAME='image_ppm_write_stream')
  USE ISO_C_BINDING, ONLY: C_INT8_T, C_INT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: stream ! FILE* expected
  INTEGER(C_INT), INTENT(IN), VALUE :: width
  INTEGER(C_INT), INTENT(IN), VALUE :: height
  INTEGER(C_INT), INTENT(IN), VALUE :: bytes_per_pixel
  INTEGER(C_INT8_T), INTENT(IN) :: buffer(*)
  INTEGER(C_INT) :: image_ppm_write_stream
END FUNCTION image_ppm_write_stream

END INTERFACE

END MODULE RSYS_IMAGE_BINDING

