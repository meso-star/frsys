! Copyright (C) |Meso|Star> 2016 (contact@meso-star.com)
!
! This software is a collection of files whose purpose is to give access
! to the RSys library from Fortran programs
!
! This software is governed by the CeCILL license under French law and
! abiding by the rules of distribution of free software. You can use,
! modify and/or redistribute the software under the terms of the CeCILL
! license as circulated by CEA, CNRS and INRIA at the following URL
! "http://www.cecill.info".
!
! As a counterpart to the access to the source code and rights to copy,
! modify and redistribute granted by the license, users are provided only
! with a limited warranty and the software's author, the holder of the
! economic rights, and the successive licensors have only limited
! liability.
!
! In this respect, the user's attention is drawn to the risks associated
! with loading, using, modifying and/or developing or reproducing the
! software by the user in light of its specific status of free software,
! that may mean that it is complicated to manipulate, and that also
! therefore means that it is reserved for developers and experienced
! professionals having in-depth computer knowledge. Users are therefore
! encouraged to load and test the software's suitability as regards their
! requirements in conditions enabling the security of their systems and/or
! data to be ensured and, more generally, to use and operate it in the
! same conditions as regards security.
!
! The fact that you are presently reading this means that you have had
! knowledge of the CeCILL license and that you accept its terms.

#include "rsys.inc"

MODULE RSYS_MEM_ALLOCATOR_BINDING

USE ISO_C_BINDING, ONLY: C_FUNPTR, C_PTR

IMPLICIT NONE

! thereafter are the function types used in the mem_allocator C struct
ABSTRACT INTERFACE

 FUNCTION alloc_fn_t(data, size, filename, fileline)
  USE ISO_C_BINDING, ONLY: C_SIZE_T, C_CHAR, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR) :: alloc_fn_t
  TYPE(C_PTR), INTENT(IN), VALUE :: data
  INTEGER(C_SIZE_T), INTENT(IN), VALUE :: size
  CHARACTER(C_CHAR), INTENT(IN) :: filename(*)
  INTEGER(C_SIZE_T), INTENT(IN), VALUE :: fileline
 END FUNCTION alloc_fn_t

 FUNCTION calloc_fn_t(data, nelts, size, filename, fileline)
  USE ISO_C_BINDING, ONLY: C_SIZE_T, C_CHAR, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR) :: calloc_fn_t
  TYPE(C_PTR), INTENT(IN), VALUE :: data
  INTEGER(C_SIZE_T), INTENT(IN), VALUE :: nelts
  INTEGER(C_SIZE_T), INTENT(IN), VALUE :: size
  CHARACTER(C_CHAR), INTENT(IN) :: filename(*)
  INTEGER(C_SIZE_T), INTENT(IN), VALUE :: fileline
 END FUNCTION calloc_fn_t

 FUNCTION realloc_fn_t(data, ptr, size, filename, fileline)
  USE ISO_C_BINDING, ONLY: C_SIZE_T, C_CHAR, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR) :: realloc_fn_t
  TYPE(C_PTR), INTENT(IN), VALUE :: data
  TYPE(C_PTR), INTENT(IN), VALUE :: ptr
  INTEGER(C_SIZE_T), INTENT(IN), VALUE :: size
  CHARACTER(C_CHAR), INTENT(IN) :: filename(*)
  INTEGER(C_SIZE_T), INTENT(IN), VALUE :: fileline
 END FUNCTION realloc_fn_t

 FUNCTION alloc_aligned_fn_t(data, size, alignment, filename, fileline)
  USE ISO_C_BINDING, ONLY: C_SIZE_T, C_CHAR, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR) :: alloc_aligned_fn_t
  TYPE(C_PTR), INTENT(IN), VALUE :: data
  INTEGER(C_SIZE_T), INTENT(IN), VALUE :: size
  INTEGER(C_SIZE_T), INTENT(IN), VALUE :: alignment
  CHARACTER(C_CHAR), INTENT(IN) :: filename(*)
  INTEGER(C_SIZE_T), INTENT(IN), VALUE :: fileline
 END FUNCTION alloc_aligned_fn_t

 SUBROUTINE rm_fn_t(data, ptr)
  USE ISO_C_BINDING, ONLY: C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: data
  TYPE(C_PTR), INTENT(IN), VALUE :: ptr
 END SUBROUTINE rm_fn_t

 FUNCTION mem_size_fn_t(data, ptr)
  USE ISO_C_BINDING, ONLY: C_SIZE_T, C_PTR
  IMPLICIT NONE
  INTEGER(C_SIZE_T) :: mem_size_fn_t
  TYPE(C_PTR), INTENT(IN), VALUE :: data
  TYPE(C_PTR), INTENT(IN), VALUE :: ptr
 END FUNCTION mem_size_fn_t

 FUNCTION allocated_size_fn_t(data)
  USE ISO_C_BINDING, ONLY: C_SIZE_T, C_PTR
  IMPLICIT NONE
  INTEGER(C_SIZE_T) :: allocated_size_fn_t
  TYPE(C_PTR), INTENT(IN), VALUE :: data
 END FUNCTION allocated_size_fn_t

 FUNCTION dump_fn_t(data, dump, max_dump_len)
  USE ISO_C_BINDING, ONLY: C_SIZE_T, C_PTR
  IMPLICIT NONE
  INTEGER(C_SIZE_T) :: dump_fn_t
  TYPE(C_PTR), INTENT(IN), VALUE :: data
  TYPE(C_PTR), INTENT(IN), VALUE :: dump
  INTEGER(C_SIZE_T), INTENT(IN), VALUE :: max_dump_len
 END FUNCTION dump_fn_t

END INTERFACE

!*****************************************************************************
! Memory allocator interface
!*****************************************************************************
 TYPE, BIND(C) :: MEM_ALLOCATOR
  TYPE(C_FUNPTR) :: ALLOC ! alloc_fn_t expected
  TYPE(C_FUNPTR) :: CALLOC ! calloc_fn_t expected
  TYPE(C_FUNPTR) :: REALLOC ! realloc_fn_t expected
  TYPE(C_FUNPTR) :: ALLOC_ALIGNED ! alloc_aligned_fn_t expected
  TYPE(C_FUNPTR) :: RM ! rm_fn_t expected
  TYPE(C_FUNPTR) :: MEM_SIZE ! mem_size_fn_t expected
  TYPE(C_FUNPTR) :: ALLOCATED_SIZE ! allocated_size_fn_t expected
  TYPE(C_FUNPTR) :: DUMP ! dump_fn_t expected

  TYPE(C_PTR) :: DATA
 END TYPE MEM_ALLOCATOR

!*****************************************************************************
! API
!*****************************************************************************

! Default allocator.
TYPE(MEM_ALLOCATOR), BIND(C, NAME='mem_default_allocator'), TARGET :: MEM_DEFAULT_ALLOCATOR

INTERFACE

!*****************************************************************************
! Regular allocation functions
!*****************************************************************************

 FUNCTION mem_alloc(size) BIND(C, NAME='mem_alloc')
  USE ISO_C_BINDING, ONLY: C_SIZE_T, C_PTR
  IMPLICIT NONE
  INTEGER(C_SIZE_T), INTENT(IN), VALUE :: size
  TYPE(C_PTR) :: mem_alloc
 END FUNCTION mem_alloc

 FUNCTION mem_calloc(nelmts, size) BIND(C, NAME='mem_calloc')
  USE ISO_C_BINDING, ONLY: C_SIZE_T, C_PTR
  IMPLICIT NONE
  INTEGER(C_SIZE_T), INTENT(IN), VALUE :: nelmts
  INTEGER(C_SIZE_T), INTENT(IN), VALUE :: size
  TYPE(C_PTR) :: mem_calloc
 END FUNCTION mem_calloc

 FUNCTION mem_realloc(ptr, size) BIND(C, NAME='mem_realloc')
  USE ISO_C_BINDING, ONLY: C_SIZE_T, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: ptr
  INTEGER(C_SIZE_T), INTENT(IN), VALUE :: size
  TYPE(C_PTR) :: mem_realloc
 END FUNCTION mem_realloc

 FUNCTION mem_alloc_aligned(size, alignment) BIND(C, NAME='mem_alloc_aligned')
  USE ISO_C_BINDING, ONLY: C_SIZE_T, C_PTR
  IMPLICIT NONE
  INTEGER(C_SIZE_T), INTENT(IN), VALUE :: size
  INTEGER(C_SIZE_T), INTENT(IN), VALUE :: alignment
  TYPE(C_PTR) :: mem_alloc_aligned
 END FUNCTION mem_alloc_aligned

 FUNCTION mem_rm(ptr) BIND(C, NAME='mem_rm')
  USE ISO_C_BINDING, ONLY: C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: ptr
  TYPE(C_PTR) :: mem_rm
 END FUNCTION mem_rm

 FUNCTION mem_size(ptr) BIND(C, NAME='mem_size')
  USE ISO_C_BINDING, ONLY: C_SIZE_T, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: ptr
  INTEGER(C_SIZE_T) :: mem_size
 END FUNCTION mem_size

 FUNCTION mem_allocated_size() BIND(C, NAME='mem_allocated_size')
  USE ISO_C_BINDING, ONLY: C_SIZE_T
  IMPLICIT NONE
  INTEGER(C_SIZE_T) :: mem_allocated_size
 END FUNCTION mem_allocated_size

!*****************************************************************************
! Proxy allocator
!*****************************************************************************
 FUNCTION mem_init_proxy_allocator(proxy, allocator) BIND(C, NAME='mem_init_proxy_allocator')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPORT MEM_ALLOCATOR
  IMPLICIT NONE
  TYPE(MEM_ALLOCATOR), INTENT(OUT) :: proxy
  TYPE(MEM_ALLOCATOR), INTENT(INOUT) :: allocator
  INTEGER(C_INT) :: mem_init_proxy_allocator
 END FUNCTION mem_init_proxy_allocator

SUBROUTINE mem_shutdown_proxy_allocator(proxy_allocator) BIND(C, NAME='mem_shutdown_proxy_allocator')
  IMPORT MEM_ALLOCATOR
  IMPLICIT NONE
  TYPE(MEM_ALLOCATOR), INTENT(IN) :: proxy_allocator
END SUBROUTINE mem_shutdown_proxy_allocator

END INTERFACE

CONTAINS

!*****************************************************************************
! Helper function
!*****************************************************************************

! checks that an allocator has no currently allocated data
! to be called at the end of a program to detect leaks
SUBROUTINE check_memory_allocator(allocator)
  USE ISO_C_BINDING, ONLY: C_CHAR, C_SIZE_T, C_LOC, C_F_PROCPOINTER
  USE ISO_FORTRAN_ENV, ONLY : ERROR_UNIT
  IMPLICIT NONE
  TYPE(MEM_ALLOCATOR), INTENT(IN) :: allocator
  INTEGER(C_SIZE_T), PARAMETER :: bufsz = 512
  CHARACTER(C_CHAR), TARGET :: buf(bufsz) = ''
  INTEGER(C_SIZE_T) :: sz

  IF (MEM_ALLOCATED_SIZE_AL(allocator) .GT. 0) THEN
    sz = MEM_DUMP_AL(allocator, C_LOC(buf), bufsz)
    write (ERROR_UNIT,*) buf
    FATAL('Memory leaks')
  ENDIF
END SUBROUTINE check_memory_allocator

!*****************************************************************************
! C Helper macros replacement
!*****************************************************************************

! mem_alloc using a given allocator
! use the macro to benefit default file name and line number for subsequent debug
#define MEM_ALLOC_AL(alloc, sz) MEM_ALLOC_(alloc, sz, __FILE__, INT(__LINE__, C_SIZE_T))

FUNCTION MEM_ALLOC_(allocator, size, filename, fileline)
  USE ISO_C_BINDING, ONLY: C_SIZE_T, C_CHAR, C_F_PROCPOINTER, C_PTR
  IMPLICIT NONE
  TYPE(MEM_ALLOCATOR), INTENT(IN) :: allocator
  INTEGER(C_SIZE_T), INTENT(IN), VALUE :: size
  CHARACTER(C_CHAR), INTENT(IN) :: filename(*)
  INTEGER(C_SIZE_T), INTENT(IN), VALUE :: fileline
  TYPE(C_PTR) :: MEM_ALLOC_
  PROCEDURE(alloc_fn_t), POINTER :: alloc

  CALL C_F_PROCPOINTER(allocator%alloc, alloc)
  MEM_ALLOC_ = alloc(allocator%data, size, filename, fileline)
END FUNCTION MEM_ALLOC_

! mem_calloc using a given allocator
! use the macro to benefit default file name and line number for subsequent debug
#define MEM_CALLOC_AL(alloc, nelt, sz) MEM_CALLOC_(alloc, nelt, sz, __FILE__, INT(__LINE__, C_SIZE_T))

FUNCTION MEM_CALLOC_(allocator, nelts, size, filename, fileline)
  USE ISO_C_BINDING, ONLY: C_SIZE_T, C_CHAR, C_F_PROCPOINTER, C_PTR
  IMPLICIT NONE
  TYPE(MEM_ALLOCATOR), INTENT(IN) :: allocator
  INTEGER(C_SIZE_T), INTENT(IN), VALUE :: nelts
  INTEGER(C_SIZE_T), INTENT(IN), VALUE :: size
  CHARACTER(C_CHAR), INTENT(IN) :: filename(*)
  INTEGER(C_SIZE_T), INTENT(IN), VALUE :: fileline
  TYPE(C_PTR) :: MEM_CALLOC_
  PROCEDURE(calloc_fn_t), POINTER :: calloc

  CALL C_F_PROCPOINTER(allocator%calloc, calloc)
  MEM_CALLOC_ = calloc(allocator%data, nelts, size, filename, fileline)
END FUNCTION MEM_CALLOC_

! mem_realloc using a given allocator
! use the macro to benefit default file name and line number for subsequent debug
#define MEM_REALLOC_AL(alloc, ptr, sz) MEM_REALLOC_(alloc, ptr, sz, __FILE__, INT(__LINE__, C_SIZE_T))

FUNCTION MEM_REALLOC_(allocator, ptr, size, filename, fileline)
  USE ISO_C_BINDING, ONLY: C_SIZE_T, C_CHAR, C_F_PROCPOINTER, C_PTR
  IMPLICIT NONE
  TYPE(MEM_ALLOCATOR), INTENT(IN) :: allocator
  TYPE(C_PTR), INTENT(IN), VALUE :: ptr
  INTEGER(C_SIZE_T), INTENT(IN), VALUE :: size
  CHARACTER(C_CHAR), INTENT(IN) :: filename(*)
  INTEGER(C_SIZE_T), INTENT(IN), VALUE :: fileline
  TYPE(C_PTR) :: MEM_REALLOC_
  PROCEDURE(realloc_fn_t), POINTER :: realloc

  CALL C_F_PROCPOINTER(allocator%realloc, realloc)
  MEM_REALLOC_ = realloc(allocator%data, ptr, size, filename, fileline)
END FUNCTION MEM_REALLOC_

! mem_alloc_aligned using a given allocator
! use the macro to benefit default file name and line number for subsequent debug
#define MEM_ALLOC_ALIGNED_AL(alloc, sz, aligt) MEM_ALLOC_ALIGNED_(alloc, sz, aligt, __FILE__, INT(__LINE__, C_SIZE_T))

FUNCTION MEM_ALLOC_ALIGNED_(allocator, size, alignment, filename, fileline)
  USE ISO_C_BINDING, ONLY: C_SIZE_T, C_CHAR, C_F_PROCPOINTER, C_PTR
  IMPLICIT NONE
  TYPE(MEM_ALLOCATOR), INTENT(IN) :: allocator
  INTEGER(C_SIZE_T), INTENT(IN), VALUE :: size
  INTEGER(C_SIZE_T), INTENT(IN), VALUE :: alignment
  CHARACTER(C_CHAR), INTENT(IN) :: filename(*)
  INTEGER(C_SIZE_T), INTENT(IN), VALUE :: fileline
  TYPE(C_PTR) :: MEM_ALLOC_ALIGNED_
  PROCEDURE(alloc_aligned_fn_t), POINTER :: alloc_aligned

  CALL C_F_PROCPOINTER(allocator%alloc_aligned, alloc_aligned)
  MEM_ALLOC_ALIGNED_ = alloc_aligned(allocator%data, size, alignment, filename, fileline)
END FUNCTION MEM_ALLOC_ALIGNED_

! mem_rm using a given allocator
SUBROUTINE MEM_RM_AL(allocator, ptr)
  USE ISO_C_BINDING, ONLY: C_SIZE_T, C_F_PROCPOINTER
  IMPLICIT NONE
  TYPE(MEM_ALLOCATOR), INTENT(IN) :: allocator
  TYPE(C_PTR), INTENT(IN), VALUE :: ptr
  PROCEDURE(rm_fn_t), POINTER :: rm

  CALL C_F_PROCPOINTER(allocator%rm, rm)
  CALL rm(allocator%data, ptr)
END SUBROUTINE MEM_RM_AL

! mem_size using a given allocator
FUNCTION MEM_SIZE_AL(allocator, ptr)
  USE ISO_C_BINDING, ONLY: C_SIZE_T, C_F_PROCPOINTER
  IMPLICIT NONE
  TYPE(MEM_ALLOCATOR), INTENT(IN) :: allocator
  TYPE(C_PTR), INTENT(IN), VALUE :: ptr
  INTEGER(C_SIZE_T) :: MEM_SIZE_AL
  PROCEDURE(mem_size_fn_t), POINTER :: mem_size

  CALL C_F_PROCPOINTER(allocator%mem_size, mem_size)
  MEM_SIZE_AL = mem_size(allocator%data, ptr)
END FUNCTION MEM_SIZE_AL

! mem_allocated_size using a given allocator
FUNCTION MEM_ALLOCATED_SIZE_AL(allocator)
  USE ISO_C_BINDING, ONLY: C_SIZE_T, C_F_PROCPOINTER
  IMPLICIT NONE
  TYPE(MEM_ALLOCATOR), INTENT(IN) :: allocator
  INTEGER(C_SIZE_T) :: MEM_ALLOCATED_SIZE_AL
  PROCEDURE(allocated_size_fn_t), POINTER :: allocated_size

  CALL C_F_PROCPOINTER(allocator%allocated_size, allocated_size)
  MEM_ALLOCATED_SIZE_AL = allocated_size(allocator%data)
END FUNCTION MEM_ALLOCATED_SIZE_AL

! mem_dump using a given allocator
FUNCTION MEM_DUMP_AL(allocator, buffer, bufsz)
  USE ISO_C_BINDING, ONLY: C_SIZE_T, C_LOC, C_F_PROCPOINTER
  IMPLICIT NONE
  TYPE(MEM_ALLOCATOR), INTENT(IN) :: allocator
  TYPE(C_PTR), INTENT(IN)  :: buffer
  INTEGER(C_SIZE_T), INTENT(IN) :: bufsz
  INTEGER(C_SIZE_T) :: MEM_DUMP_AL
  PROCEDURE(dump_fn_t), POINTER :: dump

  CALL C_F_PROCPOINTER(allocator%dump, dump)
  MEM_DUMP_AL = dump(allocator%data, buffer, bufsz)
END FUNCTION MEM_DUMP_AL

END MODULE RSYS_MEM_ALLOCATOR_BINDING
