! Copyright (C) |Meso|Star> 2016 (contact@meso-star.com)
!
! This software is a collection of files whose purpose is to give access
! to the RSys library from Fortran programs
!
! This software is governed by the CeCILL license under French law and
! abiding by the rules of distribution of free software. You can use,
! modify and/or redistribute the software under the terms of the CeCILL
! license as circulated by CEA, CNRS and INRIA at the following URL
! "http://www.cecill.info".
!
! As a counterpart to the access to the source code and rights to copy,
! modify and redistribute granted by the license, users are provided only
! with a limited warranty and the software's author, the holder of the
! economic rights, and the successive licensors have only limited
! liability.
!
! In this respect, the user's attention is drawn to the risks associated
! with loading, using, modifying and/or developing or reproducing the
! software by the user in light of its specific status of free software,
! that may mean that it is complicated to manipulate, and that also
! therefore means that it is reserved for developers and experienced
! professionals having in-depth computer knowledge. Users are therefore
! encouraged to load and test the software's suitability as regards their
! requirements in conditions enabling the security of their systems and/or
! data to be ensured and, more generally, to use and operate it in the
! same conditions as regards security.
!
! The fact that you are presently reading this means that you have had
! knowledge of the CeCILL license and that you accept its terms.

#ifndef RSYS_INC
#define RSYS_INC

! *****************************************************************************
! from rsys.h
! *****************************************************************************

! Code checking
#ifdef NDEBUG
#define ASSERT(c) ;
#else
#define ASSERT(c) IF(.NOT.(c)) THEN;WRITE (ERROR_UNIT,*) "ASSERT:", __FILE__, ":", __LINE__;ERROR STOP;ENDIF
#endif

#define CHECK(a,b) IF((a).NE.(b)) THEN;WRITE(ERROR_UNIT,*) "CHECK:", __FILE__, ":", __LINE__;ERROR STOP;ENDIF;

#define NCHECK(a,b) IF((a).EQ.(b)) THEN;WRITE(ERROR_UNIT,*) "NCHECK:", __FILE__, ":", __LINE__;ERROR STOP;ENDIF

#define CHECK_TRUE(a) IF(.NOT.(a)) THEN;WRITE(ERROR_UNIT,*) "CHECK_TRUE:", __FILE__, ":", __LINE__;ERROR STOP;ENDIF;

#define CHECK_FALSE(a) IF(a) THEN;WRITE(ERROR_UNIT,*) "CHECK_FALSE:", __FILE__, ":", __LINE__;ERROR STOP;ENDIF;

#define FATAL(msg) WRITE(ERROR_UNIT,*) msg;ERROR STOP

! *****************************************************************************
! from math.h
! *****************************************************************************

! check if 2 numbers' difference is less than eps
! use a macro to replace inline C functions
#define EQ_EPS(a, b, eps) (ABS((a) - (b)) .LE. (eps))

! *****************************************************************************
! from mem_allocator.h
! *****************************************************************************

! these macros call the corresponding functions,
! providing them with file name and line number

! mem_alloc using a given allocator
#define MEM_ALLOC_AL(alloc, sz) MEM_ALLOC_(alloc, sz, __FILE__, INT(__LINE__, C_SIZE_T))

! mem_calloc using a given allocator
#define MEM_CALLOC_AL(alloc, nelt, sz) MEM_CALLOC_(alloc, nelt, sz, __FILE__, INT(__LINE__, C_SIZE_T))

! mem_realloc using a given allocator
#define MEM_REALLOC_AL(alloc, ptr, sz) MEM_REALLOC_(alloc, ptr, sz, __FILE__, INT(__LINE__, C_SIZE_T))

! mem_alloc_aligned using a given allocator
#define MEM_ALLOC_ALIGNED_AL(alloc, sz, aligt) MEM_ALLOC_ALIGNED_(alloc, sz, aligt, __FILE__, INT(__LINE__, C_SIZE_T))

#endif
