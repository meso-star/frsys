! Copyright (C) |Meso|Star> 2015 (contact@meso-star.com)
!
! This software is a collection of files whose purpose is to give access
! to the RSys library from Fortran programs
!
! This software is governed by the CeCILL license under French law and
! abiding by the rules of distribution of free software. You can use,
! modify and/or redistribute the software under the terms of the CeCILL
! license as circulated by CEA, CNRS and INRIA at the following URL
! "http://www.cecill.info".
!
! As a counterpart to the access to the source code and rights to copy,
! modify and redistribute granted by the license, users are provided only
! with a limited warranty and the software's author, the holder of the
! economic rights, and the successive licensors have only limited
! liability.
!
! In this respect, the user's attention is drawn to the risks associated
! with loading, using, modifying and/or developing or reproducing the
! software by the user in light of its specific status of free software,
! that may mean that it is complicated to manipulate, and that also
! therefore means that it is reserved for developers and experienced
! professionals having in-depth computer knowledge. Users are therefore
! encouraged to load and test the software's suitability as regards their
! requirements in conditions enabling the security of their systems and/or
! data to be ensured and, more generally, to use and operate it in the
! same conditions as regards security.
!
! The fact that you are presently reading this means that you have had
! knowledge of the CeCILL license and that you accept its terms.

! This Fortran module has no equivalent in the original C RSys
! It gives a rudimentary access to some of the stdio.h C functions

MODULE RSYS_STDIO_BINDING

IMPLICIT NONE

INTERFACE

FUNCTION c_fopen(name, mode) BIND(C, name="fopen")
  USE ISO_C_BINDING, ONLY: C_CHAR, C_PTR
  IMPLICIT NONE
  CHARACTER(C_CHAR), INTENT(IN) :: name(*)
  CHARACTER(C_CHAR), INTENT(IN) :: mode(*)
  TYPE(C_PTR) :: c_fopen
END FUNCTION c_fopen

FUNCTION c_fclose(file) BIND(C, name="fclose")
  USE ISO_C_BINDING, ONLY: C_PTR, C_INT
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: file
  INTEGER(C_INT) :: c_fclose
END FUNCTION c_fclose

FUNCTION c_remove(name) BIND(C, name="remove")
  USE ISO_C_BINDING, ONLY: C_CHAR, C_INT
  IMPLICIT NONE
  CHARACTER(C_CHAR), INTENT(IN) :: name(*)
  INTEGER(C_INT) :: c_remove
END FUNCTION c_remove

FUNCTION c_tmpfile() BIND(C, name="tmpfile")
  USE ISO_C_BINDING, ONLY: C_PTR
  IMPLICIT NONE
  TYPE(C_PTR) :: c_tmpfile
END FUNCTION c_tmpfile

SUBROUTINE c_rewind(file) BIND(C, name="rewind")
  USE ISO_C_BINDING, ONLY: C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: file
END SUBROUTINE c_rewind

END INTERFACE

END MODULE RSYS_STDIO_BINDING
