! Copyright (C) |Meso|Star> 2016 (contact@meso-star.com)
!
! This software's purpose is to test the Fortran binding for RSys
!
! This software is governed by the CeCILL license under French law and
! abiding by the rules of distribution of free software. You can use,
! modify and/or redistribute the software under the terms of the CeCILL
! license as circulated by CEA, CNRS and INRIA at the following URL
! "http://www.cecill.info".
!
! As a counterpart to the access to the source code and rights to copy,
! modify and redistribute granted by the license, users are provided only
! with a limited warranty and the software's author, the holder of the
! economic rights, and the successive licensors have only limited
! liability.
!
! In this respect, the user's attention is drawn to the risks associated
! with loading, using, modifying and/or developing or reproducing the
! software by the user in light of its specific status of free software,
! that may mean that it is complicated to manipulate, and that also
! therefore means that it is reserved for developers and experienced
! professionals having in-depth computer knowledge. Users are therefore
! encouraged to load and test the software's suitability as regards their
! requirements in conditions enabling the security of their systems and/or
! data to be ensured and, more generally, to use and operate it in the
! same conditions as regards security.
!
! The fact that you are presently reading this means that you have had
! knowledge of the CeCILL license and that you accept its terms.

#include "rsys.inc"

! convert any number to a C_SIZE_T
#define SZT(i) INT(i, C_SIZE_T)

SUBROUTINE test_regular()
  USE ISO_C_BINDING, ONLY: C_CHAR, C_INT, C_SIZE_T, C_INTPTR_T, C_LOC, C_PTR, C_NULL_PTR, C_F_POINTER, C_ASSOCIATED
  USE RSYS_BINDING
  USE RSYS_MEM_ALLOCATOR_BINDING
  IMPLICIT NONE
  TYPE(C_PTR) :: p
  TYPE(C_PTR) :: q(3)
  CHARACTER(C_CHAR), POINTER :: PTR(:)
  INTEGER(C_SIZE_T) :: i
  TYPE(C_PTR) :: foo ! to discard unwanted function returns
  CHARACTER(C_CHAR) :: c ! for alignment computation

  p = MEM_ALLOC_ALIGNED(SZT(1024), ALIGNOF(c))
  CHECK_TRUE(C_ASSOCIATED(p))
  CHECK_TRUE(IS_ALIGNED(p, ALIGNOF(c)))
  foo = mem_rm(p)

  q(1) = mem_alloc_aligned(SZT(10), SZT(64))
  q(2) = mem_alloc(SZT(58))
  q(3) = mem_alloc(SZT(78))
  CHECK_TRUE(C_ASSOCIATED(q(1)))
  CHECK_TRUE(C_ASSOCIATED(q(2)))
  CHECK_TRUE(C_ASSOCIATED(q(3)))
  CHECK_TRUE(IS_ALIGNED(q(1), SZT(64)))

  p = mem_calloc(SZT(1), SZT(4))
  CHECK_TRUE(C_ASSOCIATED(p))
  CALL C_F_POINTER(p, PTR, [4])
  DO i = 1, 4
    CHECK(PTR(i), CHAR(0, C_CHAR))
  END DO
  DO i = 1, 4
    PTR(i) = CHAR(i, C_CHAR)
  END DO

  foo = mem_rm(q(2))

  p = mem_realloc(p, SZT(8))
  CALL C_F_POINTER(p, PTR, [8])
  DO i = 1, 4
    CHECK(PTR(i), CHAR(i, C_CHAR))
  END DO
  DO i = 5, 8
    PTR(i) = CHAR(i, C_CHAR)
  END DO

  foo = mem_rm(q(3))

  p = mem_realloc(p, SZT(5))
  CALL C_F_POINTER(p, PTR, [5])
  DO i = 1, 5
    CHECK(PTR(i), CHAR(i, C_CHAR))
  END DO

  foo = mem_rm(p)

  p = C_NULL_PTR
  p = mem_realloc(C_NULL_PTR, SZT(16))
  CHECK_TRUE(C_ASSOCIATED(p))
  p = mem_realloc(p, SZT(0))

  foo = mem_rm(q(1))

  CHECK_FALSE(C_ASSOCIATED(mem_alloc_aligned(SZT(1024), SZT(0))))
  CHECK_FALSE(C_ASSOCIATED(mem_alloc_aligned(SZT(1024), SZT(3))))
END SUBROUTINE test_regular

SUBROUTINE test_allocator(allocator)
  USE ISO_C_BINDING, ONLY: C_CHAR, C_INT, C_SIZE_T, C_LOC, C_PTR, C_NULL_PTR, C_F_POINTER, C_ASSOCIATED
  USE RSYS_BINDING
  USE RSYS_MEM_ALLOCATOR_BINDING
  IMPLICIT NONE
  CHARACTER(C_CHAR), TARGET :: dump(24)
  TYPE(MEM_ALLOCATOR), INTENT(IN) :: allocator
  TYPE(C_PTR) :: p
  TYPE(C_PTR) :: q(3)
  CHARACTER(C_CHAR), POINTER :: PTR(:)
  INTEGER(C_SIZE_T) :: i
  INTEGER(C_SIZE_T) :: foo ! to discard unwanted function returns

  p = MEM_ALLOC_ALIGNED_AL(allocator, SZT(1024), SZT(1))
  CHECK_TRUE(C_ASSOCIATED(p))
  CHECK_TRUE(IS_ALIGNED(p, SZT(1)))
  CALL MEM_RM_AL(allocator, p)

  q(1) = MEM_ALLOC_ALIGNED_AL(allocator, SZT(10), SZT(8))
  q(2) = MEM_CALLOC_AL(allocator, SZT(1), SZT(58))
  q(3) = MEM_ALLOC_AL(allocator, SZT(78))
  CHECK_TRUE(C_ASSOCIATED(q(1)))
  CHECK_TRUE(C_ASSOCIATED(q(2)))
  CHECK_TRUE(C_ASSOCIATED(q(3)))
  CHECK_TRUE(IS_ALIGNED(q(1), SZT(8)))

  p = MEM_CALLOC_AL(allocator, SZT(2), SZT(2))
  CHECK_TRUE(C_ASSOCIATED(p))
  CALL C_F_POINTER(p, PTR, [4])
  DO i = 1, 4
    CHECK(PTR(i), CHAR(0, C_CHAR))
  END DO
  DO i = 1, 4
    PTR(i) = CHAR(i, C_CHAR)
  END DO

  foo = MEM_DUMP_AL(allocator, C_LOC(dump), SZT(24))
  write(*,*) dump
  foo = MEM_DUMP_AL(allocator, C_LOC(dump), SZT(16))
  write(*,*) dump
  foo = MEM_DUMP_AL(allocator, C_NULL_PTR, SZT(0)) ! may not crash

  CALL MEM_RM_AL(allocator, q(2))

  p = MEM_REALLOC_AL(allocator, p, SZT(8))
  CALL C_F_POINTER(p, PTR, [8])
  DO i = 1, 4
    CHECK(PTR(i), CHAR(i, C_CHAR))
  END DO
  DO i = 5, 8
    PTR(i) = CHAR(i, C_CHAR)
  END DO

  CALL MEM_RM_AL(allocator, q(3))

  p = MEM_REALLOC_AL(allocator, p, SZT(5))
  CALL C_F_POINTER(p, PTR, [5])
  DO i = 1, 5
    CHECK(PTR(i), CHAR(i, C_CHAR))
  END DO

  CALL MEM_RM_AL(allocator, p)

  p = C_NULL_PTR
  p = MEM_REALLOC_AL(allocator, C_NULL_PTR, SZT(16))
  CHECK_TRUE(C_ASSOCIATED(p))
  p = MEM_REALLOC_AL(allocator, p, SZT(0))

  CALL MEM_RM_AL(allocator, q(1))

  CHECK_FALSE(C_ASSOCIATED(mem_alloc_aligned(SZT(1024), SZT(0))))
  CHECK_FALSE(C_ASSOCIATED(mem_alloc_aligned(SZT(1024), SZT(3))))
  CHECK(MEM_ALLOCATED_SIZE_AL(allocator), 0)
END SUBROUTINE test_allocator

PROGRAM MAIN
  USE RSYS_BINDING
  USE RSYS_MEM_ALLOCATOR_BINDING
  USE ISO_C_BINDING, ONLY: C_INT, C_LOC
  IMPLICIT NONE
  TYPE(mem_allocator) :: allocator
  INTEGER(C_INT), TARGET :: mem(8)

  write(*,*) '-- Common allocation functions'
  CALL test_regular()

  write(*,*) '-- Default allocator'
  CALL test_allocator(mem_default_allocator)

  write(*,*) '-- Proxy allocator'
  CHECK(MEM_INIT_PROXY_ALLOCATOR(allocator, mem_default_allocator), RES_OK)
  CALL test_allocator(allocator)
  CALL mem_shutdown_proxy_allocator(allocator)

  CHECK_FALSE(MEM_AREA_OVERLAP(C_LOC(mem), SZT(2)*SIZEOF(mem(1)), C_LOC(mem(3)), SZT(6)*SIZEOF(mem(1))))
  CHECK_FALSE(MEM_AREA_OVERLAP(C_LOC(mem(5)), SZT(4)*SIZEOF(mem(1)), C_LOC(mem), SZT(4)*SIZEOF(mem(1))))
  CHECK_TRUE(MEM_AREA_OVERLAP(C_LOC(mem), SZT(2)*SIZEOF(mem(1)), C_LOC(mem(2)), SZT(7)*SIZEOF(mem(1))))
  CHECK_TRUE(MEM_AREA_OVERLAP(C_LOC(mem(8)), SZT(1)*SIZEOF(mem(1)), C_LOC(mem), SZT(8)*SIZEOF(mem(1))))

  CHECK(MEM_ALLOCATED_SIZE_AL(mem_default_allocator), 0)
END PROGRAM
